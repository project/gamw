Google Ad Manager Workspace
=======================

Project site: http://drupal.org/project/gamw

Code: https://drupal.org/project/gamw/git-instructions

Issues: https://drupal.org/project/issues/gamw

INTRODUCTION
------------

This module aims to make adding Google Ads to any Drupal 8 website a simple and
effective process. It removes the need for any boilerplate JS or HTML code that
is required by Google Ad Manager and allows you to handle everything from the
UI.

This module can be used by even the most inexperienced Drupal users to
successfully add Google Ads to their website while abiding by all coding
standards set in the Google Ad Manager documentation.

CONFIGURATION
-------------

The module will offer a configuration form that can be accessed at
"/admin/gamw/settings". Most of the field descriptions in that form are pretty
self explanatory but I will go through them nonetheless.

- **Google Ad Manager Account ID (Required)**
This is your Google Ad Manager Account ID that you can retrieve from Google Ad
Manager.

- **Site Name (Required)**
This is a site-wide key:value targeting attribute which is useful if you have
the same ad units playing on multiple sites and want to distinguish them.
If you don't already have this targeting attribute I highly suggest you add it
in your Google Ad Manager. This is the only targeting attribute that comes by
default with the module and its key is 'site'. This 'site' targeting attribute
will be automatically added to all your ads on this site.

- **Enable developer mode (Optional)**
This will output all the ad objects in the console log of your browser.
This is useful for debugging ad blocks.

- **Collapse empty divs (Optional)**
This will collapse the advertisement area to 0x0 pixels if no advertisement is
served to avoid empty space. Internally this runs the
googletag.pubads().collapseEmptyDivs() function.

- **Ad Unit .csv file (Required)**
This field requests a .csv file upload that contains all the ad unit information
that you can export from Google Ad Manager. In Google Ad Manager go to
Inventory > Ad units , then filter so that the results contain the ad units that
will be playing on this site. Do not select any of them, simply filter for them
and then click the "Download" button at the top to retrieve the .csv file. From
this file I retrieve all the ad unit machine names as well as what creative
sizes each of them accepts. If you wish to add / remove ad units to the site
repeat this process with those ad units added to the .csv file. This field,
along with the account ID field, are the most important in this form. Everything
depends on these two.

- **Ad Size Breakpoint(s) (Optional)**
This field will only show if you have uploaded a .csv file in the "Ad Unit .csv
file" field. Input width breakpoint sizes in pixels, comma separated, to load
extra fields (multiple option select fields) that allow you to designate which
creative sizes play on what device screen widths. Please note that these values
refer to the minimum width value. (eg. If you enter 768 then you will be given
an option to select sizes that will play from 768px and above until overriden by
a higher breakpoint.) Always begin with a 0px breakpoint and go up from there.

- **Creative sizes dynamic fields (Required)**
These fields will only show if you have added breakpoint values in the "Ad Size
Breakpoint(s)" field. Select as many creative sizes as you wish to play on each
breakpoint. The title of these fields will show you the breakpoint you are
choosing for. The creative size options are aggregated from ALL the ad units
using the .csv file.

Once you are done setting the module configurations all you need to do is go to
Structure > Block layout and place "GAMW Advertisement" blocks in whatever
region you please. The block form will let you choose which ad unit this block
will be rendering and you can also add targeting attributes to further target
Google Ad Manager Line items depending on any values you have set in Google Ad
Manager from Inventory > Key-Values.

INSTALLATION
------------

The Google Ad Manager Workspace project installs like any other Drupal module
There is extensive documentation on how to do this here:
https://drupal.org/documentation/install/modules-themes/modules-8

But essentially:
1. a) The preferred way would be using composer by simply running "composer
require drupal/gamw".

2. b) Or you could download the tarball and expand it into the modules/
directory in your Drupal 8 installation.

1. Within Drupal, enable the GAMW module in Admin menu > Extend.

2. Enjoy Google Ads on your website.

If you find a problem, incorrect comment, obsolete or improper code or such,
please search for an issue about it at http://drupal.org/project/issues/gamw
If there isn't already an issue for it, please create a new one.

Thanks.

REQUIREMENTS
------------

Knowledge on Google Ad Manager.
Basic knowledge on Drupal module installation/configuration and block placement.

MAINTAINERS
-----------

Current maintainers:
 - Elias Papa - https://www.drupal.org/u/eliaspapa
