/**
 * @file
 */
/* eslint no-console: ["error", { allow: ["warn", "error"] }] */
(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.gamw = {
    attach(context, settings) {
      $('div[id^="gamw"]', context).each(function () {
        let key;
        let key2;
        let key3;
        const all_sizes = [];
        const uuid = $(this).attr('data-uuid');
        const ad_unit = $(this).attr('data-ad-unit');
        const gamw_settings = settings.gamw.blocks[ad_unit][uuid];
        const sizes_object = {};
        for (key in gamw_settings.sizes) {
          if (gamw_settings.sizes.hasOwnProperty(key)) {
            for (key2 in gamw_settings.sizes[key]) {
              if (gamw_settings.sizes[key].hasOwnProperty(key2)) {
                const array2 = gamw_settings.sizes[key][key2];
                sizes_object[key2] = array2;
              }
            }
          }
        }
        for (key3 in sizes_object) {
          if (sizes_object.hasOwnProperty(key3)) {
            all_sizes.push(sizes_object[key3]);
          }
        }

        Drupal.gamw.googletag.cmd.push(() => {
          const mapping = Drupal.gamw.googletag.sizeMapping();
          $.each(gamw_settings.sizes, (index, value) => {
            const mapping_sizes = [];
            for (const key in value) {
              if (value.hasOwnProperty(key)) {
                mapping_sizes.push(value[key]);
              }
            }
            mapping.addSize([parseInt(index), 0], mapping_sizes);
          });
          const mapping_actual = mapping.build();

          const slot_definition = Drupal.gamw.googletag.defineSlot(
            `/${gamw_settings.gamw_id}/${gamw_settings.ad_unit}`,
            all_sizes,
            `gamw_${gamw_settings.uuid}`
          );
          if (typeof slot_definition !== 'undefined') {
            slot_definition
              .defineSizeMapping(mapping_actual)
              .addService(Drupal.gamw.googletag.pubads());
            $.each(gamw_settings.targeting_attributes, (index, value) => {
              slot_definition.setTargeting(index, value);
            });
            Drupal.gamw.googletag.gptAdSlots.push(slot_definition);
          }
          Drupal.gamw.googletag.pubads().enableSingleRequest();
          if (gamw_settings.collapsed) {
            Drupal.gamw.googletag.pubads().collapseEmptyDivs();
          }
          Drupal.gamw.googletag.enableServices();
          Drupal.gamw.googletag.display(`gamw_${gamw_settings.uuid}`);
          if (gamw_settings.dev_mode) {
            console.warn(`GAMW Advertisement-(${ad_unit}): `, gamw_settings);
          }
        });
      });
    }
  };
})(jQuery, Drupal);
