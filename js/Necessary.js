/**
 * @file
 */

var googletag = googletag || {};
(function ($, Drupal) {
  'use strict';
  Drupal.gamw = Drupal.gamw || {};
  Drupal.gamw.googletag = googletag;
  Drupal.gamw.googletag.cmd = googletag.cmd || [];
  Drupal.gamw.googletag.gptAdSlots = [];
  var gads = document.createElement('script');
  gads.async = true;
  var useSSL = document.location.protocol === 'https:';
  gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
  var node = document.getElementsByTagName('script')[0];
  node.parentNode.insertBefore(gads, node);
})(jQuery, Drupal);
