<?php

namespace Drupal\gamw\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Gamw' block.
 *
 * @Block(
 *  id = "gamw",
 *  admin_label = @Translation("GAMW Advertisement"),
 * )
 */
class Gamw extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Component\Uuid\UuidInterface definition.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidDi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->uuidDi = $container->get('uuid');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $ad_unit_information = $this->configFactory->get('gamw.settings')->get('ad_unit_information');
    $ad_unit_sizes_per_breakpoint = $this->configFactory->get('gamw.settings')->get('ad_unit_sizes_default');
    $site_name = $this->configFactory->get('gamw.settings')->get('site_name');
    $gamw_id = $this->configFactory->get('gamw.settings')->get('gamw_id');
    $dev_mode = $this->configFactory->get('gamw.settings')->get('dev_mode');
    $collapsed = $this->configFactory->get('gamw.settings')->get('collapse');
    $ad_sizes = [];
    $ad_sizes_split = [];
    $integer_dimensions = [];
    foreach ($ad_unit_sizes_per_breakpoint['creative_sizes'] as $breakpoint => $sizes) {
      $ad_sizes[$breakpoint] = array_intersect($sizes, $ad_unit_information[$this->configuration['ad_unit']]);
      foreach ($ad_sizes as $key => $value) {
        foreach ($value as $key2 => $value2) {
          $dimension_split = explode('x', $value2);
          for ($i = 0; $i < count($dimension_split); $i++) {
            $integer_dimensions[$i] = (int) $dimension_split[$i];
          }
          $ad_sizes_split[$key][$key2] = $integer_dimensions;
        }
      }
    }

    $targeting_attributes = [];
    foreach ($this->configuration['attributes_wrapper'] as $value) {
      if ($value) {
        $split_value = explode(':', $value);
        $targeting_attributes[$split_value[0]] = $split_value[1];
      }
    }
    $targeting_attributes['site'] = $site_name;

    $uuid = $this->uuidDi->generate();

    $build['#attached']['library'][] = 'gamw/ad';
    $build['#cache']['max-age'] = 0;
    $build['gamw'] = [
      '#type' => 'markup',
      '#markup' => '<div id="gamw_' . $uuid . '" data-uuid="' . $uuid . '" data-ad-unit="' . $this->configuration['ad_unit'] . '"></div>',
    ];
    $build['#attached']['drupalSettings']['gamw']['blocks'][$this->configuration['ad_unit']][$uuid] = [
      'uuid' => $uuid,
      'ad_unit' => $this->configuration['ad_unit'],
      'sizes' => $ad_sizes_split,
      'dev_mode' => $dev_mode,
      'collapsed' => $collapsed,
      'targeting_attributes' => $targeting_attributes,
      'gamw_id' => $gamw_id,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'ad_unit' => [],
      'attributes_wrapper' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $ad_unit_information = $this->configFactory->get('gamw.settings')->get('ad_unit_information');
    $options = array_combine(array_keys($ad_unit_information), array_keys($ad_unit_information));

    $form['ad_unit'] = [
      '#type' => 'select',
      '#description' => $this->t('Select the ad unit.'),
      '#title' => $this->t('Ad Unit'),
      '#options' => $options,
      '#default_value' => $this->configuration['ad_unit'],
      '#required' => TRUE,
    ];

    $form['attributes_wrapper'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => ['id' => 'key-attribute-wrapper'],
    ];

    $num_attributes = $form_state->get('num_attributes');
    if (!$num_attributes) {
      $num_attributes = count($this->configuration['attributes_wrapper']) ? count($this->configuration['attributes_wrapper']) : 1;
      $form_state->set('num_attributes', $num_attributes);
    }

    for ($i = 0; $i < $num_attributes; $i++) {
      if (!isset($form['attributes_wrapper'][$i])) {
        $form['attributes_wrapper'][$i] = [
          '#type'  => 'textfield',
          '#title' => $this->t('Extra targeting attribute @number', ['@number' => $i + 1]),
          '#default_value' => $this->configuration['attributes_wrapper'][$i],
          '#description' => $this->t('Add an extra targeting attribute in the format <strong>key:value</strong>.<br />The <strong>value</strong> can be multiple comma-separated string values.'),
        ];
      }
    }

    $form['attributes_wrapper']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another targeting attribute'),
      '#submit' => [
      [$this, 'addAttributeSubmit'],
      ],
      '#ajax' => [
        'callback' => [$this, 'returnAttributesAjax'],
        'wrapper' => 'key-attribute-wrapper',
      ],
    ];

    $form['attributes_wrapper']['remove'] = [
      '#type' => 'submit',
      '#value' => $this->t('Remove last targeting attribute'),
      '#submit' => [
      [$this, 'removeAttributeSubmit'],
      ],
      '#ajax' => [
        'callback' => [$this, 'returnAttributesAjax'],
        'wrapper' => 'key-attribute-wrapper',
      ],
    ];

    return $form;
  }

  /**
   * Submit callback for adding attributes.
   */
  public function addAttributeSubmit(array &$form, FormStateInterface $form_state) {
    $num_attributes = $form_state->get('num_attributes') + 1;
    $form_state->set('num_attributes', $num_attributes);
    $form_state->setRebuild();
  }

  /**
   * Submit callback for removing attributes.
   */
  public function removeAttributeSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->get('num_attributes') != 1) {
      $num_attributes = $form_state->get('num_attributes') - 1;
      $form_state->set('num_attributes', $num_attributes);
      $form_state->setRebuild();
    }
  }

  /**
   * Ajax callback for the attributes.
   */
  public function returnAttributesAjax(array &$form, FormStateInterface $form_state) {
    return $form['settings']['attributes_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['ad_unit'] = $form_state->getValue('ad_unit');
    $this->configuration['attributes_wrapper'] = $form_state->getValue('attributes_wrapper');
  }

}
