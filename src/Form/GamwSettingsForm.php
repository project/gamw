<?php

namespace Drupal\gamw\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\FileInterface;

/**
 * Configure gamw settings for this site.
 */
class GamwSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  private $fileStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->fileStorage = $instance->entityTypeManager->getStorage('file');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gamw_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gamw.settings');

    $form['gamw_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Google Ad Manager Account ID'),
      '#description' => $this->t('You can find this ID in your Google Ad Manager Account.'),
      '#default_value' => $config->get('gamw_id'),
      '#required' => TRUE,
    ];

    $form['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Name'),
      '#description' => $this->t("This is a site-wide key:value targeting attribute which is useful if you have the same ad units playing on multiple sites and want to distinguish them.<br />If you don't already have this targeting attribute I highly suggest you add it in your Google Ad Manager.<br />This is the only targeting attribute that comes by default with the module and its key is '<strong>site</strong>'.<br />This '<strong>site</strong>' targeting attribute will be automatically added to all your ads on this site."),
      '#default_value' => $config->get('site_name'),
      '#required' => TRUE,
    ];

    $form['dev_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable developer mode.'),
      '#description' => $this->t('This will output all the ad objects in the console log of your browser as warnings.'),
      '#default_value' => $config->get('dev_mode'),
    ];

    $form['collapse'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Collapse empty divs.'),
      '#description' => $this->t('This will collapse the advertisement area to 0x0 pixels if no advertisement is served to avoid empty space.'),
      '#default_value' => $config->get('collapse'),
    ];

    $form['ad_unit_csv_file_id'] = [
      '#title' => $this->t('Ad Unit .csv file'),
      '#attributes' => [
        'name' => 'ad_unit_csv_file_id',
      ],
      '#type' => 'managed_file',
      '#description' => $this->t('Export the ad units you wish to use from Google Ad Manager and upload the .csv file here.'),
      '#upload_location' => 'public://' . date('Y-m'),
      '#default_value' => $config->get('ad_unit_csv_file_id'),
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
    ];

    $form['breakpoints'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ad Size Breakpoint(s)'),
      '#description' => $this->t("Input width breakpoint sizes in pixels, <strong>comma separated</strong>, to load extra fields (multiple option select fields) that allow you to designate which creative sizes play on what device screen widths.<br />Please note that these values refer to the <strong>minimum</strong> width value.(eg. If you enter 768 then you will be given an option to select sizes that will play from 768px and above until overriden by a higher breakpoint.)<br /><strong>Always begin with a 0px breakpoint and go up from there.</strong>"),
      '#default_value' => $config->get('breakpoints'),
      '#pattern' => '^[0-9,]+$',
      '#ajax' => [
        'callback' => '::chooseBreakpointSizes',
        'wrapper' => 'breakpoint-wrapper',
        'event' => 'change',
        'effect' => 'fade',
      ],
      '#states' => [
        'visible' => [
          ':input[name="ad_unit_csv_file_id[fids]"]' => ['filled' => TRUE],
        ],
      ],
    ];

    $form['breakpoint_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'breakpoint-wrapper'],
    ];

    $ad_unit_csv_file_id = $form_state->getValue('ad_unit_csv_file_id') ? $form_state->getValue('ad_unit_csv_file_id') : $config->get('ad_unit_csv_file_id');
    $csv_contents_array = $form_state->get('csv_contents');
    if (!$csv_contents_array && $ad_unit_csv_file_id) {
      $csv_file_id = $ad_unit_csv_file_id[0];
      $csv_file = $this->fileStorage->load($csv_file_id);
      if ($csv_file && $csv_file instanceof FileInterface) {
        $file_entity_uri = $csv_file->getFileUri();
        $file_pointer = fopen($file_entity_uri, "r");
        $csv_contents_array = [];
        if ($file_pointer) {
          while (($line = fgetcsv($file_pointer)) !== FALSE) {
            $csv_contents_array[] = $line;
          }
          fclose($file_pointer);
          $form_state->set('csv_contents', $csv_contents_array);
        }
      }
    }

    $ad_unit_information = [];
    $ad_unit_sizes = [];
    if ($csv_contents_array) {
      foreach ($csv_contents_array as $key => $row) {
        if ($key != 0) {
          $sizes = explode(';', $row[4]);
          $ad_unit_code = $row[2];
          foreach ($sizes as $key => $size) {
            $size = trim($size);
            $ad_unit_information[$ad_unit_code][$size] = $size;
            $ad_unit_sizes[$size] = $size;
            $form_state->set('ad_unit_information', $ad_unit_information);
            $form_state->set('ad_unit_sizes', $ad_unit_sizes);
          }
        }
      }
    }

    $breakpoints = $form_state->getValue('breakpoints');
    $breakpoints = trim($breakpoints, ',');
    if (!empty($breakpoints) || $config->get('ad_unit_sizes_default')['creative_sizes']) {
      if (!empty($breakpoints)) {
        $temp = explode(',', $breakpoints);
        $breakpoint_array = [];
        foreach ($temp as $key => $value) {
          $breakpoint_array[$value] = $value;
        }
      }
      else {
        $breakpoint_array = $config->get('ad_unit_sizes_default')['creative_sizes'];
      }

      $form['breakpoint_wrapper']['#tree'] = TRUE;

      foreach ($breakpoint_array as $key => $value) {
        $form['breakpoint_wrapper']['creative_sizes'][$key] = [
          '#type' => 'select',
          '#title' => $this->t('Creative sizes for above @breakpointpx', ['@breakpoint' => $key]),
          '#options' => $ad_unit_sizes,
          '#default_value' => $config->get('ad_unit_sizes_default')['creative_sizes'][$key],
          '#multiple' => TRUE,
          '#required' => TRUE,
          '#states' => [
            'visible' => [
              ':input[name="breakpoints"]' => ['filled' => TRUE],
            ],
          ],
        ];
      }
    }
    $form['#attached']['library'][] = 'gamw/styling';

    return parent::buildForm($form, $form_state);
  }

  /**
   * Ajax callback for the breakpoint sizes.
   */
  public function chooseBreakpointSizes(array $form, FormStateInterface $form_state) {
    if ($form_state->getValue('breakpoints')) {
      return $form['breakpoint_wrapper'];
    }
    else {
      $config = $this->config('gamw.settings');
      $config->clear('ad_unit_sizes_default');
      $config->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('gamw.settings')
        // Set the submitted configuration setting.
      ->set('gamw_id', $form_state->getValue('gamw_id'))
      ->set('site_name', $form_state->getValue('site_name'))
      ->set('ad_unit_csv_file_id', $form_state->getValue('ad_unit_csv_file_id'))
      ->set('breakpoints', $form_state->getValue('breakpoints'))
      ->set('dev_mode', $form_state->getValue('dev_mode'))
      ->set('collapse', $form_state->getValue('collapse'))
      ->set('ad_unit_information', $form_state->get('ad_unit_information'))
      ->set('ad_unit_sizes', $form_state->get('ad_unit_sizes'))
      ->save();

    if (array_key_exists('creative_sizes', $form['breakpoint_wrapper'])) {
      foreach ($form['breakpoint_wrapper']['creative_sizes'] as $key => $value) {
        if (is_numeric($key)) {
          $this->configFactory->getEditable('gamw.settings')
            ->set('ad_unit_sizes_default', $form_state->getValue('breakpoint_wrapper'))
            ->save();
        }
      }
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gamw.settings',
    ];
  }

}
